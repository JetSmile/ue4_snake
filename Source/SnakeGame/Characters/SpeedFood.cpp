// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedFood.h"

#include "Kismet/GameplayStatics.h"

ASpeedFood::ASpeedFood()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASpeedFood::BeginPlay()
{
	AFood::BeginPlay();
}

void ASpeedFood::Tick(float DeltaTime)
{
	AFood::Tick(DeltaTime);
}

void ASpeedFood::Interact(AActor* Interactor, bool bIsHead)
{
	Super::Interact(Interactor, bIsHead);
	
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), UGameplayStatics::GetGlobalTimeDilation(GetWorld()) + 0.5f);
}
