#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	Up,
	Down,
	Left,
	Right
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY(EditDefaultsOnly)
	float ElementSize = 100.f;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed = 10.f;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection = EMovementDirection::Down;
protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(TSubclassOf<ASnakeElementBase> SnakeClass, int ElementsNum = 1, bool bIsInit = false);

	UFUNCTION(BlueprintCallable)
	void Move();
	
	UFUNCTION(BlueprintCallable)
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
};
