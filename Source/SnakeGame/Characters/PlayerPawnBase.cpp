#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>("PawnCamera");
	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (Value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::Down)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Up;
		}
		else if (Value < 0&& SnakeActor->LastMoveDirection != EMovementDirection::Up)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Down;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (Value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::Left)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Right;
		}
		else if (Value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::Right)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::Left;
		}
	}
}
