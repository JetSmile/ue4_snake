#include "Damage.h"
#include "SnakeBase.h"

ADamage::ADamage()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ADamage::BeginPlay()
{
	Super::BeginPlay();
}

void ADamage::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

