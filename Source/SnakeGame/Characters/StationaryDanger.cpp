#include "StationaryDanger.h"
#include "SnakeBase.h"

AStationaryDanger::AStationaryDanger()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AStationaryDanger::BeginPlay()
{
	Super::BeginPlay();
}

void AStationaryDanger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AStationaryDanger::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

