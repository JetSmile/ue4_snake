#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Kismet/KismetMathLibrary.h"

ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(SnakeElementClass, 5, true);
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(TSubclassOf<ASnakeElementBase> SnakeClass, int ElementsNum, bool bIsInit)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation;
		FRotator NewRotation;
		if (bIsInit)
		{
			NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
			NewRotation = UKismetMathLibrary::MakeRotFromX(FVector(-1, 0, 0));
		}
		else
		{
			ASnakeElementBase* LastElement = SnakeElements[SnakeElements.Num() - 1];
			ASnakeElementBase* PrevLastElement = SnakeElements[SnakeElements.Num() - 2];

			FVector LocationLastElement = LastElement->GetActorLocation();
			FVector PrevLocationLastElement = PrevLastElement->GetActorLocation();
			const FVector NewLocationLastElem = LocationLastElement + LocationLastElement - PrevLocationLastElement;
			LastElement->SetActorLocation(NewLocationLastElem);

			NewRotation = LastElement->GetActorRotation();
			NewLocation = LocationLastElement;
		}

		const FTransform STransform(NewRotation, NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeClass, STransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex;
		if (bIsInit)
		{
			ElementIndex = SnakeElements.Add(NewSnakeElement);
		}
		else
		{
			ElementIndex = SnakeElements.Num() - 1;
			SnakeElements.Insert(NewSnakeElement, ElementIndex);
		}

		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FRotator MovementRotation;
	FVector MovementVector(0, 0, 0);
	const float MovementActorSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::Up:
		MovementVector.X += MovementActorSpeed;
		MovementRotation = UKismetMathLibrary::MakeRotFromX(FVector(1, 0, 0));
		break;
	case EMovementDirection::Down:
		MovementVector.X -= MovementActorSpeed;
		MovementRotation = UKismetMathLibrary::MakeRotFromX(FVector(-1, 0, 0));
		break;
	case EMovementDirection::Left:
		MovementVector.Y -= MovementActorSpeed;
		MovementRotation = UKismetMathLibrary::MakeRotFromX(FVector(0, -1, 0));
		break;
	case EMovementDirection::Right:
		MovementVector.Y += MovementActorSpeed;
		MovementRotation = UKismetMathLibrary::MakeRotFromX(FVector(0, 1, 0));
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		ASnakeElementBase* CurrentElement = SnakeElements[i];
		ASnakeElementBase* PrevElement = SnakeElements[i - 1];
		const FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);

		const FRotator PrevRotation = PrevElement->GetActorRotation();
		CurrentElement->SetActorRotation(PrevRotation);
	}

	SnakeElements[0]->SetActorRotation(MovementRotation);
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}
