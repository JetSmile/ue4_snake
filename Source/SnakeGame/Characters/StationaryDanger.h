#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "StationaryDanger.generated.h"

UCLASS()
class SNAKEGAME_API AStationaryDanger : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AStationaryDanger();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
