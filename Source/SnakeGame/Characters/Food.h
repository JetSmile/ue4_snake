#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeGame/Spawner.h"
#include "Food.generated.h"

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AFood();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpawner> SpawnerClass;

	void SetDamage(ADamage* Damage);
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

protected:
	ASpawner* Spawner;

	ADamage* DamageFood;
};
